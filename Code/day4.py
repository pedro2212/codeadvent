#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

file = "day4.txt"
listentries = ('byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid')

f = open(file, "r")
data = f.read()
passports = data.split('\n\n')  # split strings by the blank lines


def checkpassportpart1():
    invalids = 0
    for i in passports:
        for y in listentries:
            if not re.search(y + ':', i):  # when it does not find one the fields
                invalids += 1
                break
    return len(passports) - invalids  # valids


def checkpassportpart2():
    valids = 0
    for i in passports:
        validations_bool = list()
        validationbyr = re.search('byr:([1][9][2-9][0-9]|[2][0][0][0-2])', i)  # between 1920 and 2002
        validationiyr = re.search('iyr:([2][0][1][0-9]|[2][0][2][0])', i)  # between 2010 and 2020
        validationeyr = re.search('eyr:([2][0][2][0-9]|[2][0][3][0])', i)  # between 2020 and 2030
        validationhgt = re.search('hgt:(([1][5-8][0-9]|[1][9][0-3])cm|([5][9]|[6][0-9]|[7][0-6])in)', i)  # between 150 and 193cm, 59 and 76 in
        validationhcl = re.search('hcl:#\w{6}( |\n|$)', i)  # starts with # and 6 alfanumerics afterwards
        validationecl = re.search('ecl:(amb|blu|brn|gry|grn|hzl|oth)', i)  # amb,blu,brn,gry,grn,hzl or oth
        validationpid = re.search('pid:\d{9}( |\n|$)', i)  # 9 digits

        validations = (validationbyr, validationiyr, validationeyr, validationhgt, validationhcl, validationecl,
                       validationpid)  # store the results
        for z in validations:
            validations_bool.append(False) if z is None else validations_bool.append(True)  # convert to booleans
        if all(validations_bool):  # when it does find all the fields
            valids += 1
    return valids  # valids


okpassports = checkpassportpart2()
print('There are %i valid passports' % (okpassports))

