#!/usr/bin/env python
# -*- coding: utf-8 -*-

file = 'day6.txt'

f = open(file, "r")
data = f.read()
answers = data.split('\n\n')

answersp1 = [x.replace('\n', '') for x in answers]


def getanswerspart1():

    sum = 0

    for i in answersp1:
        letterlist = list()
        for z in i:
            if z not in letterlist:  # search uniques
                letterlist.append(z)  # append unique
        value = len(letterlist)
        sum += value

    return sum


def getanswerspart2():
    valids = 0
    valid = False
    for index, i in enumerate(answers):  # cycles through group
        x = i.split('\n')
        for letter in x[0]:  # cycle through the letters of the first entry only
            for ind, line in enumerate(x):  # cycles through the lines of each group
                if ind != 0:  # does not go to the first one
                    if letter in line:
                        valid = True
                    else:
                        valid = False  # letter not contained in one of the lines, break the loop, it is invalid
                        break
            if valid:
                valids += 1
    return valids



finalvalue = getanswerspart2()
print(finalvalue)

