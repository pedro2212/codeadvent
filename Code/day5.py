#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import math

file = "day5.txt"

with open(file) as f:
    lines = [line.rstrip() for line in f]  # takes the \n

rows = 128
columns = 8


def excludefirsthalf(arr):
    """
    :param arr: array of integers
    :return: second half of the array
    """
    arr = list(range(arr[len(arr) // 2], arr[len(arr) - 1] + 1))
    return arr


def excludesecondhalf(arr):
    """
    :param arr: array of integers
    :return: first half of the array
    """
    arr = list(range(arr[0], arr[len(arr) // 2]))
    return arr


def convertseatids(var, way):
    """
    converts seats to sequential ids
    :param var: variable representing array with row and column or just a integer (if s2r)
    :return:
    """

    if way == "r2s":  # real to sequential
        value = int(str(var[0]) + str(var[1]))  # getting row + column to put in the formula
        row = var[0] + 1  # getting row
        seatid = value - (row * 2 - 3)  # seat 20 (row 3, column 1) will have id 20 - (3*2-3) = 17
                                                # formula is specific for 8 columns
    elif way == 's2r':
        row = math.ceil(var/columns)  # getting row by multiples of columns
        seatid = var + (row * 2 - 3)  # convert it to real number
    return seatid


def checkhseatpart1():
    hseatid = 0
    for i in lines:
        arrayrows = list(range(rows))
        arraycolumns = list(range(columns))

        for z in i:
            if z == 'F':
                arrayrows = excludesecondhalf(arrayrows)
            elif z == 'B':
                arrayrows = excludefirsthalf(arrayrows)
            elif z == 'L':
                arraycolumns = excludesecondhalf(arraycolumns)
            elif z == 'R':
                arraycolumns = excludefirsthalf(arraycolumns)

        seatid = arrayrows[0] * 8 + arraycolumns[0]  # arrays will end up with just 1 element, which will be
                                                    # row and column
        if seatid > hseatid:  # check highest id
            hseatid = seatid
        else:
            continue
    return hseatid


def checkhseatpart2():
    seats = list()
    seatids = list()
    for i in lines:
        arrayrows = list(range(rows))
        arraycolumns = list(range(columns))

        for z in i:
            if z == 'F':
                arrayrows = excludesecondhalf(arrayrows)
            elif z == 'B':
                arrayrows = excludefirsthalf(arrayrows)
            elif z == 'L':
                arraycolumns = excludesecondhalf(arraycolumns)
            elif z == 'R':
                arraycolumns = excludefirsthalf(arraycolumns)

        seatid = (arrayrows[0], arraycolumns[0])
        seats.append(seatid)  # register every seat in a list, [2, 2] will be row 3, column 3

    for x in seats:
        seatids.append(convertseatids(var=x, way='r2s'))  # converts real seatids to sequential seatids

    seatids.sort()

    for index, y in enumerate(seatids):
        if seatids[index + 1] - seatids[index] != 1:  # finds the number missing in the sequence
            missingseat = convertseatids(var=seatids[index + 1] - 1, way='s2r')  # reconverts it to real id
            rowmissingseat = missingseat // 10
            columnmissingseat = int(str(missingseat)[-1])
            break
    return rowmissingseat * 8 + columnmissingseat


seatid = checkhseatpart2()
print('Seat id is %i' % (seatid))