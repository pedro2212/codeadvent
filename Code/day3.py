#!/usr/bin/env python
# -*- coding: utf-8 -*-

file = "day3.txt"

with open(file) as f:
    lines = [line.rstrip() for line in f]  # takes the \n

lengthline = len(lines[0])


def counttreespart1(length):
    x = 1  # initial position x
    incrementx = 3  # increment x per line (per y)
    tree = 0  # initialize number of trees found

    for index, i in enumerate(lines):
        if index == 0:  # first line doesn't matter
            continue
        x += incrementx  # add movement x axis per line
        if x > length:
            x = x - length  # refactor x when it exceeds length of line
        if i[x - 1] == '#':  # finding a tree
            tree += 1
    return tree


def counttreespart2(length, incrementx, incrementy):
    x = 1  # initial position x
    tree = 0  # initialize number of trees found

    for index, i in enumerate(lines):
        if index == 0 or index % incrementy != 0:  # skip first line and lines that will be skipped with change in y
            continue
        x += incrementx  # add movement x axis per line read
        if x > length:
            x = x - length  # refactor x when it exceeds length of line
        if i[x - 1] == '#':  # finding a tree
            tree += 1
    return tree


slope1 = counttreespart2(length=lengthline, incrementx=1, incrementy=1)
slope2 = counttreespart2(length=lengthline, incrementx=3, incrementy=1)
slope3 = counttreespart2(length=lengthline, incrementx=5, incrementy=1)
slope4 = counttreespart2(length=lengthline, incrementx=7, incrementy=1)
slope5 = counttreespart2(length=lengthline, incrementx=1, incrementy=2)

print(slope1*slope2*slope3*slope4*slope5)