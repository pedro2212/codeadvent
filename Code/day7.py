# -*- coding: utf-8 -*-
import numpy as np
import math
import re

file = "day7.txt"

with open(file) as f:
    lines = [line.rstrip() for line in f]  # takes the \n


def searchlines(lines, bag2find):
    """
    :param lines:
    :param bag2find: is a list of the bags we need to find in the containings
    :return: bags that contain elements of bag2find
    """
    baglist = list()
    for i in lines:
        splitted = i.split()
        bag = " ".join([splitted[0], splitted[1]])  # get the name of the bag
        containings = " ".join(splitted[3:])  # get what is in the bag

        for x in bag2find:
            if x in containings:
                baglist.append(bag)  # if it is in the bag, append to the list
    return baglist


def searchbagspart1(baglist):
    finalbaglist = list()
    while len(baglist) > 0:  # the baglist will grow and then shrink to 0, starts with one bag in this case
        baglist = searchlines(lines, baglist)
        for z in baglist:
            finalbaglist.append(z)

    finalbaglist = list(set(finalbaglist))  # retrieves only the uniques

    return len(finalbaglist)

def parsebag(line):
    splitted = line.split()
    main_bag = " ".join([splitted[0], splitted[1]])  # get the name of the bag
    containings = " ".join(splitted[3:])  # get what is in the bag
    return main_bag, containings

def searchbagspart2(total,iter_number, iter_bag):
    for i in lines:
        main_bag, containings = parsebag(i)
        if iter_bag == main_bag:
            findings = re.findall('\d .*?(?:(?!bag).)*', containings)
            if not findings:  # the bag does not contain any more bags
                return total
            for j in findings:
                number = int(re.findall('\d', j)[0])  # gets the number of bags
                bag = (re.findall('\D+', j)[0]).strip()  # gets the bag name
                total = total + iter_number * number  # adds to the total number of bags
                total = searchbagspart2(total=total, iter_number=iter_number * number, iter_bag=bag)
    return total


baglist = 'shiny gold'
total = 0
numberbags = searchbagspart2(total=total,iter_bag=baglist,iter_number=1)

print(numberbags)