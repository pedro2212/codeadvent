#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time

file = "day1.txt"

with open(file) as f:
    lines = [line.rstrip() for line in f]  # takes the \n


start = time.time()


def findnumberspart1():
    """
    Finds two numbers that are 2020 when summed, and multiplies them
    :return:
    """
    for i in lines:
        first = int(i)
        for j in lines:
            second = int(j)
            sum = first + second
            if sum == 2020:
                print('The first number is %i and the second is %i' % (first, second))
                return first * second


def findnumberspart2():
    """
    Finds three numbers that are 2020 when summed, and multiplies them
    :return:
    """
    for i in lines:
        first = int(i)
        for j in lines:
            second = int(j)
            sumtwo = first + second
            if sumtwo > 2020:
                continue
            else:
                for z in lines:
                    third = int(z)
                    sumthree = sumtwo + third
                    if sumthree == 2020 and first != second and second != third:
                        return first * second * third


finalnumber = findnumberspart2()
print('The final number is %i' % (finalnumber))

end = time.time()
print('This took %f seconds' % ((end - start)))