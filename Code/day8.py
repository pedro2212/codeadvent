# -*- coding: utf-8 -*-
import numpy as np
import math
import re

file = "day8.txt"

with open(file) as f:
    lines = [line.rstrip() for line in f]  # takes the \n

def sequencepart1():
    list_steps = list()
    i = 0
    acc = 0

    while i not in list_steps:
        list_steps.append(i)
        splitted = lines[i].split()
        if splitted[0] == 'nop':
            i += 1
        elif splitted[0] == 'acc':
            acc += int(splitted[1])
            i += 1
        elif splitted[0] == 'jmp':
            i += int(splitted[1])
    return acc

def sequencepart2(nop2jump, jump2nop):
    list_steps = list()
    i = 0
    acc = 0
    index_nop = 0
    index_jmp = 0

    while i < len(lines):
        if i in list_steps:
            return False  # if a step repeats, break and return False
        list_steps.append(i)
        splitted = lines[i].split()
        if splitted[0] == 'nop' and nop2jump == index_nop:  # convert a nop to a jump
            i += int(splitted[1])
            index_nop += 1
        elif splitted[0] == 'nop':
            i += 1
            index_nop += 1
        elif splitted[0] == 'acc':
            acc += int(splitted[1])
            i += 1
        elif splitted[0] == 'jmp' and jump2nop == index_jmp:  # convert a jump to a nop
            i += 1
            index_jmp += 1
        elif splitted[0] == 'jmp':
            i += int(splitted[1])
            index_jmp += 1
    return acc


def changefunction():
    result = False
    j = 0
    while not result:
        result = sequencepart2(jump2nop=j, nop2jump=-1)  # sets the j th jump to a nop
        if not result:
            result = sequencepart2(nop2jump=j, jump2nop=-1)  # sets the j th nop to a jump
        j += 1
    return result



acc = changefunction()
print(acc)