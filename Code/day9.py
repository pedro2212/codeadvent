#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import math

file = "day9.txt"

with open(file) as f:
    lines = [line.rstrip() for line in f]  # takes the \n

init = 25


def part1():
    for i in range(0, len(lines)):
        count = 0
        mov_array = list(lines[i:i + init])
        element_after = int(lines[i + init])
        for index, j in enumerate(mov_array):
            j = int(j)
            for z in range(index + 1, len(mov_array)):
                y = int(mov_array[z])
                if y + j == element_after:
                    count += 1
        if count == 0:
           return element_after


def part2():
    target = part1()
    sum = 0
    init_index = 0
    final_index = 0
    max = 0
    min = 0
    for index, j in enumerate(lines):
        sum += int(j)
        if sum > target:
            sum = 0
            init_index = index + 1
            next
        elif sum == target:
            final_index = index + 1
            final_array = lines[init_index:final_index]
            final_array = list(map(int, final_array))
            max = max(final_array)
            min = min(final_array)
            return target, max, min


target,max, min = part2()
print(target, max, min)




