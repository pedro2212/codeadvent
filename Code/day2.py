#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time

file = "day2.txt"

with open(file) as f:
    lines = [line.rstrip() for line in f]  # takes the \n


def validpasswordspart1():
    valids = 0
    for i in lines:
        string = i.split(' ')  # gets what is in between the spaces

        numbers = string[0].split('-')  # gets the two numbers
        lvalue = int(numbers[0])   # lower limit value
        hvalue = int(numbers[1])   # higher limit value

        letter = string[1][0]  # gets the letter
        password = string[2]  # gets the password

        reps = password.count(letter)  # counts number of occurences of the letter in the password

        if hvalue >= reps >= lvalue:  # compares with lower and higher limits
            valids += 1
    return valids


def validpasswordspart2():
    valids = 0
    for i in lines:
        reps = 0
        string = i.split(' ')  # gets what is in between the spaces

        numbers = string[0].split('-')  # gets the two numbers
        firstvalue = int(numbers[0])   # lower limit value
        secondvalue = int(numbers[1])   # higher limit value

        letter = string[1][0]  # gets the letter
        password = string[2]  # gets the password

        if password[firstvalue - 1] == letter:  # checks if letter occurs in first index
            reps += 1
        if password[secondvalue - 1] == letter:  # checks if letter occurs in second index
            reps += 1

        if reps == 1:  # reps must be 1 to be valid
            valids += 1
    return valids


okpasswords = validpasswordspart2()
print('Number of valid passwords: %i' %(okpasswords))

